# Prerequisites

Here is all the stuff you need to have installed!

## A terminal
We will be doing most if not all of our donderdag on the terminal. So make sure you have one, and that it works as you want it to.

On mac:
```
Hit apple+space and type terminal and you should find it. Tweak as per your convenience.
```
On windows your options are:

1. `Hit the windows key and start typing cmd -> default windows terminal`
2. `Install [git bash](https://www.makeuseof.com/install-git-git-bash-windows/)`.
**Please note: We will be copy and pasting stuff from and to the command line. This is akward in many windows terminals. I personally dislike gitbash a bit less than the default terminal. Check that you know how to do this. Your future self will be grateful.**

On linux:
If you use linux you probably have this covered.

## Git
Check if git is installed and works by running:
```
git --version
```
which should answer something like:
```
git version 2.31.1
```

If it does not install git on your [windows machine](https://medium.com/devops-with-valentine/2021-how-to-install-git-on-windows-10-step-by-step-guide-1c9db500e734) or [mac](https://www.igeeksblog.com/how-to-install-git-on-mac/) (I recommend using Homeberew) and try again.

## Docker and docker-compose

If you have docker and docker compose installed, verify that it is working correctly:

```
docker --version
```
Should output something like:
```
Docker version 20.10.7, build v20.10.7
```
And also:
```
docker-compose --version
```
should output something like:
```
docker-compose version 1.29.2, build unknown
```
or
```
Docker compose version v2.5.1
```

If you do not have docker installed please do so. 
Follow the instructions for [mac](https://docs.docker.com/desktop/mac/install/) , [windows](https://docs.docker.com/desktop/windows/install/) or if you are using linux, then you are probably able to find the correct istructions for your distro.

## Take notes
We will talk about stuff, I will write and draw on the board, and we'll run many commands with different options.

You will ***definitely learn the most if you write your own notes and create your own docker glossary***

If you like writing / drawing by hand bring a note book, but be sure to also have a way to take digital notes. Copy and paste from and to the command line will be your friend!




