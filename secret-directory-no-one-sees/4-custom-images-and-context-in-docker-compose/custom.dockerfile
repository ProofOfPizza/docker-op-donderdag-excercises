FROM node:18
WORKDIR /app
COPY *.json ./
COPY src ./src/
RUN npm install \
  && npx tsc
CMD ["node", "dist/app.js"]
