# Docker Op Donderdag

This is a short guide through *Docker op donderdag*. It outlines the prerequisites and how to follow along. And we'll mention the basic topics we will cover as well as the main questions we hope to answer.

## Prerequisites
To follow along please make sure your system meets all the [prerequisites](./prerequisites.md)

## How to follow along
This afternoon is designed to be highly interactive. We will mob-learn here. This means that:
1. At any given time someone of us will be sharing the screen. We'll rotate so you might get lucky!
2. If something is unclear, or things go too fast: Ask! You are probably not the only one, so be nice to your colleagues and ask!
3. Take notes! Write your own notes, it will give you the best learning experience. You can use this file, and add your notes under the relevant headers, or use your own files or templates. Whatever you do: take notes because it will be way more than you'll be able to remember in one go.
4. Have fun... if you like fun at least. :)

## Topics and Main Questions
The most important topics and questions for tonight will be:

1. What is Docker ?
2. A tiny bit of history
3. What are Docker images ?
4. What are Docker containers ?
5. Docker basics: `run`, `exec`, `logs`,  and looking at and managing Docker resources.
6. The Dockerfile and its basic commands: `FROM`, `COPY`, `RUN`, `WORKDIR`, `CMD`, `ENTRYPOINT` and `build`
7. Docker build and runtime arguments: `ARG`, `ENV`
8. Data persistence: `volumes` and `bind mounts`
9. Simplify life with docker-compose: `version`, `service`, `build`, `environment` and more.
10. Docker networking: `ports`, `network` and `inspect`
11. Build custom images in docker compose and context

### What is Docker ?

### A tiny bit of history

### What are Docker images ?

### What are Docker containers ?

### Docker basics: `run`, `exec`, `logs`,  and looking at and managing Docker resources.

### The Dockerfile and its basic commands: `FROM`, `COPY`, `RUN`, `WORKDIR`, `CMD`, `ENTRYPOINT` and `build`

### Docker build and runtime arguments: `ARG`, `ENV`

### Data persistence: `volumes` and `bind mounts`

### Simplify life with docker-compose: `version`, `service`, `build`, `environment` and more.

### Docker networking: `ports`, `network` and `inspect`

### Build custom images in docker-compose and context


## Glossary of commands
Copy and paste your commands, options and summary of explanations here. :)
