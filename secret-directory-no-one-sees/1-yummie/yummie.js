const colors = {
  red: "\x1b[31m%s\x1b[0m",
  green: "\x1b[32m%s\x1b[0m",
  yellow: "\x1b[33m%s\x1b[0m",
  blue: "\x1b[34m%s\x1b[0m",
  magenta: "\x1b[35m%s\x1b[0m",
  cyan: "\x1b[36m%s\x1b[0m",
};
const envColor = process.env["COLOR"];
const color = envColor ? colors[envColor.toLowerCase()] : false;
log = (text) => (color ? console.log(color, text) : console.log(text));

const args = process.argv.slice(2);
yumm = async () => {
  for (let i = 50; i >= 0; i--) {
    if (i > 0) {
      res = log(
        `I am so happy, so so happy! I have ${i} strawberries left! I think I'll eat one!`
      );
      await new Promise((res) => {
        setTimeout(res, 40);
      });
    } else {
      log(`I am so so fulllllllllll ! `);
      args.forEach((arg) => {
        log(`... I ate all the ${arg}`);
      });
    }
  }
};

yumm();

