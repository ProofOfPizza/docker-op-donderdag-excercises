#!/bin/bash

INPUT_FILE_NAME=./output/file.txt
if [[ ! -d "./output" ]]
then
  mkdir output
fi
if [[ ! -f "${INPUT_FILE_NAME}" ]]
then
  touch "${INPUT_FILE_NAME}"
fi

readarray -t INPUT_FILE < "${INPUT_FILE_NAME}"

for LINE in "${INPUT_FILE[@]}"
do
  echo "${LINE}"
done

while true
do
  INPUT_FILE=()
  readarray -t INPUT_FILE < "${INPUT_FILE_NAME}"

  echo "Want to add something ? Y/N"
  read CONTINUE
  if [[ "${CONTINUE}" == "n" || "${CONTINUE}" == "N" ]]
  then
    break
  fi
  echo "What then ?"
  read ADDITION
  INPUT_FILE=()
  readarray -t INPUT_FILE < "${INPUT_FILE_NAME}"
  INPUT_FILE+=( "${ADDITION}" )
  printf "%s\n" "${INPUT_FILE[@]}" > "${INPUT_FILE_NAME}"
  echo "exit Y/N ?"
  for LINE in "${INPUT_FILE[@]}"
  do
    echo "${LINE}"
  done
done

